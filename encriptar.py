#encoding=UTF-8
from tkinter import *
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from complemento import whoami, desactivar_botones_si_nada_seleccionado
from actualizar import update
from error_montar import montar_error

def montar_proyecto(tabla_proyectos, btn_1, btn_2, btn_3):
	password_enc = StringVar()
	
	encriptar = Toplevel()
	encriptar.geometry("500x210")
	encriptar.minsize(width=500, height=210)
	encriptar.maxsize(width=500, height=210)
	encriptar.configure(bg='#D1D6D6')
	encriptar.title("Montar proyecto")
	
	def pedir_contrasena():
		encriptar.destroy()
		solicitud_passwd = Toplevel()
		solicitud_passwd.geometry("300x210")
		solicitud_passwd.minsize(width=300, height=210)
		solicitud_passwd.maxsize(width=300, height=210)
		solicitud_passwd.configure(bg="#D1D6D6")
		
		def mount ():
			seleccionado = tabla_proyectos.selection()	
			item = [tabla_proyectos.item(seleccionado)["text"]]
			updated_item = str(item)[2:-2]
			print (updated_item)
			user = whoami()
			passwd = password_enc.get()
			print(passwd)
			directorio_base = os.path.join("/home", user, "Proyectos")
			dir_desencriptado = os.path.join(directorio_base, updated_item)
			dir_encriptado = os.path.join(directorio_base, "." + updated_item + "_enc")
			print (dir_desencriptado)
			print (dir_encriptado)
			montar = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
			input_string = "{passwd}\n".format(passwd = password_enc.get())
			print(input_string)
			prueba24 = montar.communicate(input= input_string.encode("utf-8"))[0]
			prueba24
			solicitud_passwd.destroy()
			update(tabla_proyectos)
			montar_error(tabla_proyectos, item, dir_desencriptado)
			desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)
		
		lbl3 = Label(solicitud_passwd, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial",18))
		lbl3.place(x=90, y=30)
		
		contraseña160 = Entry (solicitud_passwd, fg='black', textvariable=password_enc, bg="white", font=("Arial", 12))
		contraseña160.place(x=50, y=90, width=200)
		
		btn3 = Button(solicitud_passwd, text="Realizar", bg="black", fg="white", command = mount)
		btn3.place(x=200, y=150)
		
	seleccionado = tabla_proyectos.selection()	
	item = [tabla_proyectos.item(seleccionado)["text"]]
	updated_item = str(item)[2:-2]
	print (updated_item)
	
	texto_confirmacion = "¿Desea montar el proyecto " + updated_item + "?"
	print (texto_confirmacion)
	confirmacion = Label(encriptar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text= texto_confirmacion)
	confirmacion.place(x=50, y=40, width=400)
	
	btn77 = Button(encriptar, text="Sí", bg="black", fg="white", command=pedir_contrasena)
	btn77.place(x=280, y=120, width=75)
	
	def destroyer():
		encriptar.destroy()
		
	btn78 = Button(encriptar, text="No", bg="black", fg="white", command=destroyer)
	btn78.place(x=140, y=120, width=75)
