from tkinter import *
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from complemento import whoami, desactivar_botones_si_nada_seleccionado
from actualizar import update
from error_desmontar import desmontar_error

def desmontar_proyecto(tabla_proyectos, btn_1, btn_2, btn_3):
	def umount ():
		seleccionado = tabla_proyectos.selection()	
		item = [tabla_proyectos.item(seleccionado)["text"]]
		updated_item = str(item)[2:-2]
		print (updated_item)
		user = whoami()
		directorio_base = os.path.join("/home", user, "Proyectos")
		dir_desencriptado = os.path.join(directorio_base, updated_item)
		print (dir_desencriptado)
		subprocess.call(["umount", dir_desencriptado])
		desmontar.destroy()
		update(tabla_proyectos)
		desmontar_error(tabla_proyectos, item, dir_desencriptado)
		desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)

	desmontar = Toplevel()
	desmontar.geometry("500x210")
	desmontar.minsize(width=500, height=210)
	desmontar.maxsize(width=500, height=210)
	desmontar.configure(bg='#D1D6D6')
	desmontar.title("Desmontar proyecto")
	
	seleccionado = tabla_proyectos.selection()	
	item = [tabla_proyectos.item(seleccionado)["text"]]
	updated_item = str(item)[2:-2]
	print (updated_item)
	
	texto_confirmacion = "¿Desea desmontar el proyecto " + updated_item + "?"
	print (texto_confirmacion)
	confirmacion = Label(desmontar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text= texto_confirmacion)
	confirmacion.place(x=50, y=40, width=400)
	
	btn77 = Button(desmontar, text="Sí", bg="black", fg="white", command=umount)
	btn77.place(x=280, y=120, width=75)
	
	def destroyer():
		desmontar.destroy()
		
	btn78 = Button(desmontar, text="No", bg="black", fg="white", command=destroyer)
	btn78.place(x=140, y=120, width=75)
