#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from complemento import whoami, desactivar_botones_si_nada_seleccionado
from crear import crear_proyecto
from desencriptar import desmontar_proyecto
from encriptar import montar_proyecto
from eliminar import del_project
from actualizar import update

user = whoami()
directorio_base = os.path.join("/home", user, "Proyectos")
subprocess.call(["mkdir", directorio_base])

window = Tk()
window.title("Aplicación Zymvol")
window.configure(bg="#D1D6D6")
window.geometry("1280x720")
window.minsize(width=700, height=600)
window.maxsize(width=1920, height=1080)

lbl = Label(window, text="Nombre de la aplicación", font=("Arial", 25), bg="#2193D5", fg="black", anchor="w")
lbl.place(relx=0.17, rely=0.1, relwidth=0.665)

estilo = ttk.Style()
estilo.configure("mystyle.Treeview", font=("Arial", 12))
estilo.configure("mystyle.Treeview.Heading", font=("Arial", 16))

tree_frame = Frame(window)
tree_frame.place(relx=0.17, rely=0.35, relwidth=0.665, relheight=0.4)

tabla_proyectos = ttk.Treeview(tree_frame, selectmode=BROWSE, columns=("Ubicación", "Status","Backups"), style="mystyle.Treeview")
tabla_proyectos.place(relwidth=1, relheight=1)
tabla_proyectos.column("#0", width=130)
tabla_proyectos.column("#2", width=115)
tabla_proyectos.heading("#0", text="ID")
tabla_proyectos.heading("#1", text="Ubicación")
tabla_proyectos.heading("#2", text="Status") 
tabla_proyectos.heading("#3", text="Último backup")
	
verscrlbar = ttk.Scrollbar(tree_frame, orient="vertical")	
verscrlbar.pack(side="right", fill="y")

tabla_proyectos.configure(yscrollcommand=verscrlbar.set)

update(tabla_proyectos)
	
btn = Button (window, text="Crear proyecto", font=("Arial", 11), bg="black", fg="white", command=lambda: crear_proyecto(tabla_proyectos, btn_1, btn_2, btn_3))
btn.place(relx=0.17, rely=0.18)

btn_1 = Button (window, text="Montar proyecto", font=("Arial",11), bg="gray", fg="black", command=lambda:montar_proyecto(tabla_proyectos, btn_1, btn_2, btn_3))
btn_1.place(relx=0.731, rely=0.18)

btn_2 = Button (window, text="Desmontar proyecto", font=("Arial", 11), bg="gray", fg="black", command=lambda:desmontar_proyecto(tabla_proyectos, btn_1, btn_2, btn_3))
btn_2.place(relx=0.711, rely=0.24)

btn_3 = Button (window, text="Eliminar proyecto", font=("Arial",11), bg="gray", fg="black", command=lambda:del_project(tabla_proyectos, btn_1, btn_2, btn_3))
btn_3.place(relx=0.17, rely=0.24)

desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)

def activar_botones (event):
	q = tabla_proyectos.selection()
	qu = [tabla_proyectos.item(q)["text"]]
	que = str(qu)
	var_vacia = str("[\'\']")
	if (que != var_vacia):
		btn_1["state"] = NORMAL
		btn_2["state"] = NORMAL
		btn_3["state"] = NORMAL
		btn_1.configure(bg="black", fg="white")
		btn_2.configure(bg="black", fg="white")
		btn_3.configure(bg="black", fg="white")
		print ("Los botones se han activado.")

tabla_proyectos.bind("<<TreeviewSelect>>", activar_botones)
	
window.mainloop()
