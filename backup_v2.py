#!/usr/bin/python3
import os
import subprocess
from subprocess import PIPE
import shutil
import getpass
from complemento import whoami

# Después de LOGNAME escribir: START_HOURS_RANGE=3-22 (esto hará que si el equipo esta activo más de un día entero, entre el horario de 3 a.m hasta 22 p.m se podrá vovler a ejecutar el script, actua como cron más la ventaja de anacron)

#escribir en anacron : 1	30	backup		su - tu_usuario -c 'python3 direccion_completa_de_este_archivo' (esto hará que se ejecuta una vez al dia cada dia, tardará 30 minutos en proceder desde que el equipo se inicia, el nombre de la tarea será backup y lo siguiente es el comando

def backup():	
	user = whoami()
	directorio_proyectos = os.path.join("/home", user, "Proyectos")
	directorio_raiz_backups = os.path.join("/home", user, "backups")
	listado_proyectos = os.listdir(directorio_proyectos)
	direcciones_montadas = []
	
	def comprobacion_montados(direcciones_montadas, proyecto, comprobacion_proyectos_montados_formato_texto,directorio_raiz_backups):
		if direcciones_montadas[-1] in str(comprobacion_proyectos_montados_formato_texto):
			proyecto_comprimido = proyecto + ".zip"
			dir_backups_proyectos = os.path.join(directorio_raiz_backups, proyecto_comprimido)
			subprocess.call(["rm", "-rf", dir_backups_proyectos])
			shutil.make_archive(directorio_entero_backups, "zip", directorio_raiz_backups, proyecto)
			print("IT WORKS!!!")
		
	for proyecto in listado_proyectos:
		
		if (proyecto[0] != "." and proyecto[-4:] != "_enc"):
			print(proyecto)
			directorio_entero_proyectos = os.path.join(directorio_proyectos, proyecto)
			directorio_entero_backups = os.path.join(directorio_raiz_backups, proyecto)
			direcciones_montadas.append(directorio_entero_proyectos)
#			Comprobación de que el proyecto esta montado para realizar el backup y comprimirlo, si ya existe, primero se eliminará y se cambiará por el nuevo para actualizarlo.
			comprobacion_proyectos_montados = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
			comprobacion_proyectos_montados_formato_texto = str(comprobacion_proyectos_montados.communicate())	
			comprobacion_montados(direcciones_montadas, proyecto, comprobacion_proyectos_montados_formato_texto, directorio_raiz_backups)
backup()
