#encoding=UTF-8
from tkinter import *
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from complemento import whoami, desactivar_botones_si_nada_seleccionado
from actualizar import update
from error_crear import crear_error
from caracteres_especiales import correcion

def crear_proyecto(tabla_proyectos, btn_1, btn_2, btn_3):
	new_window = Toplevel()
	new_window.geometry("500x400")
	new_window.configure(bg="#D1D6D6")
	lbl10 = Label(new_window, text="¿Tipo de proyecto?", font=("Arial", 20), bg="#D1D6D6", fg="black")
	new_window.minsize(width=500, height=400)
	new_window.maxsize(width=500, height=400)

	lbl10.place(x=20, y=20)

	opciones = ttk.Combobox(new_window,
		        state="readonly")
	opciones["values"] = ["Proyecto A", "Proyecto B"]

	opciones.set("Seleccionar")
	opciones.place(x=20, y=80)

	lbl33 = Label(new_window, text="Según el tipo de proyecto que selecciones, A o B, aqui se mostrará su correspondiente definición y contenido.", bg="white", fg="black", font=("Arial", 10), wraplength=137, justify="left")
    	
	lbl33.place(x=310, y=80, height=150)
    	
	def modified(event):
		
		if (event.widget.get() == "Proyecto A"):
			lbl33.config(text="Al seleccionar el Proyecto A, va a obtener las siguientes carpetas: Docs, Scripts, Inputs, Outputs")
			
			def A():
				password = StringVar()	
				ID = StringVar()
				usuario_in = StringVar()
				passwordlab_in = StringVar()

				def ejecutar():
					user = whoami()
					ID_proyecto = ID.get()
					print (ID_proyecto)
					directorio_base = os.path.join("/home", user, "Proyectos")
					print (directorio_base)
					subprocess.call(["mkdir", directorio_base])
					dir_desencriptado = os.path.join(directorio_base, ID_proyecto)
					print (dir_desencriptado)
					dir_encriptado = os.path.join (directorio_base, "." + ID_proyecto + "_enc")
					print (dir_encriptado)
					carpeta_1 = os.path.join(dir_desencriptado, "output")
					carpeta_2 = os.path.join(dir_desencriptado, "input")
					carpeta_3 = os.path.join(dir_desencriptado, "docs")
					carpeta_4 = os.path.join(dir_desencriptado, "scripts")
					passwd = password.get()
					
					zymvol = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
					input_string = "y\ny\n{passwd}\n{passwd}\n".format(passwd = password.get())
					print (input_string)
					prueba24 = zymvol.communicate(input= input_string.encode("utf-8"))[0]
					prueba24
					subprocess.Popen(["mkdir", carpeta_1, carpeta_2, carpeta_3, carpeta_4])
					proyecto_a.destroy()
					correcion(ID_proyecto)
											
					tabla_proyectos.insert("",0, text=ID_proyecto, values=(dir_desencriptado, "Montado"))
					crear_error(tabla_proyectos, ID_proyecto, dir_desencriptado)
					desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)

				proyecto_a = Toplevel()
				proyecto_a.geometry("500x300")
				proyecto_a.configure(bg="#D1D6D6")
				proyecto_a.title("Proyecto A")
				proyecto_a.minsize(width=500, height=300)
				proyecto_a.maxsize(width=500, height=300)

				new_window.destroy()
				lbl = Label(proyecto_a, text="Configuración", bg="#D1D6D6", fg="black", font=("Arial", 18))
				lbl.place(x=75, y= 25)

				lbl2 = Label(proyecto_a, text="Nombre del proyecto:", bg="#D1D6D6", fg="black", font=("Arial", 12))
				lbl2.place(x=75, y= 80)

				lbl1 = Label(proyecto_a, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial", 12))
				lbl1.place(x=75, y=150)
				
				nombre_proyecto = Entry (proyecto_a, fg="black", font="Arial", textvariable=ID)
				nombre_proyecto.place(x=75, y=105)
					
				contraseña_entry = Entry(proyecto_a, fg="black", font="Arial", textvariable=password)
				contraseña_entry.place(x=75, y= 175)

				btn = Button(proyecto_a, text="Realizar", bg="black", fg="white", command = ejecutar)
				btn.place(x=400, y= 250)
				
			btn32.config(command=A)
		    
		if (event.widget.get() == "Proyecto B"):
			lbl33.config(text="Al seleccionar el Proyecto B, va a obtener las siguientes carpetas: HTML, CSS, JavaScript")

			def B():
				password2 = StringVar()	
				ID2 = StringVar()
				usuario_in2 = StringVar()
				passwordlab_in2 = StringVar()

				def ejecutar2():
					user = whoami()
					ID_proyecto = ID2.get()
					usuario = usuario_in2.get()

					print (ID_proyecto)
					directorio_base = os.path.join("/home", user, "Proyectos")
					print (directorio_base)
					subprocess.call(["mkdir", directorio_base])
					dir_desencriptado = os.path.join(directorio_base, ID_proyecto)
					print (dir_desencriptado)
					dir_encriptado = os.path.join (directorio_base, "." + ID_proyecto + "_enc")
					print (dir_encriptado)
					carpeta_1 = os.path.join(dir_desencriptado, "JavaScript")
					carpeta_2 = os.path.join(dir_desencriptado, "HTML")
					carpeta_3 = os.path.join(dir_desencriptado, "CSS")
					carpeta_4 = os.path.join(dir_desencriptado, "scripts")
					passwd = password2.get()
					
					zymvol = subprocess.Popen(["encfs", "-S", dir_encriptado, dir_desencriptado], stdout=PIPE, stdin=PIPE)
					input_string = "y\ny\n{passwd}\n{passwd}\n".format(passwd = password2.get())
					print (input_string)
					prueba24 = zymvol.communicate(input= input_string.encode("utf-8"))[0]
					prueba24
					subprocess.Popen(["mkdir", carpeta_1, carpeta_2, carpeta_3, carpeta_4])
					proyecto_b.destroy()
					 
					tabla_proyectos.insert("",0, text=ID_proyecto, values=(dir_desencriptado, "Montado"))
					crear_error(tabla_proyectos, ID_proyecto, dir_desencriptado)
					desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)

				proyecto_b = Toplevel()
				proyecto_b.geometry("500x300")
				proyecto_b.configure(bg="#D1D6D6")
				proyecto_b.title("Proyecto B")
				proyecto_b.minsize(width=500, height=300)
				proyecto_b.maxsize(width=500, height=300)
				new_window.destroy()

				lbl = Label(proyecto_b, text="Configuración", bg="#D1D6D6", fg="black", font=("Arial", 18))
				lbl.place(x=75, y= 25)

				lbl2 = Label(proyecto_b, text="Nombre del proyecto:", bg="#D1D6D6", fg="black", font=("Arial", 12))
				lbl2.place(x=75, y= 80)

				lbl1 = Label(proyecto_b, text="Contraseña:", bg="#D1D6D6", fg="black", font=("Arial",12))
				lbl1.place(x=75, y=150)
				
				nombre_proyecto = Entry (proyecto_b, fg="black", font="Arial", textvariable=ID2)
				nombre_proyecto.place(x=75, y=105)
					
				contraseña_entry2 = Entry(proyecto_b, fg="black", font="Arial", textvariable=password2)
				contraseña_entry2.place(x=75, y= 175)

				btn = Button(proyecto_b, text="Realizar", bg="black", fg="white", command = ejecutar2)
				btn.place(x=400, y= 250)
				

			btn32.config(command=B)
            
	opciones.bind('<<ComboboxSelected>>', modified)

	btn32 = Button(new_window, text="Siguiente", bg="black", fg="white", font=("Arial", 10))
	btn32.place(x=410, y=350)
