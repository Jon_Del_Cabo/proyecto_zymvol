#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from complemento import whoami
from actualizar import update

def crear_error(tabla_proyectos, ID_proyecto, dir_desencriptado):
    status = 0       
    def mostrar_no_error_crear(status):  
        if status == "Montado":
            crear_funciona = Toplevel()
            crear_funciona.geometry("500x150")
            crear_funciona.minsize(width=500, height=150)
            crear_funciona.maxsize(width=500, height=150)
            crear_funciona.configure(bg="#D1D6D6")

            lbl178 = Label(crear_funciona, text="¡El proyecto se ha creado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl178.place(x=60, y=50)
            def destroyer ():
                crear_funciona.destroy()
            btn297 = Button(crear_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
        else:      
            print("**error**")   
            crear_error = Toplevel()
            crear_error.geometry("500x150")
            crear_error.minsize(width=500, height=150)
            crear_error.configure(bg="#D1D6D6")
            lbl278 = Label(crear_error, text="¡El proyecto no se ha podido crear!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl278.place(x=60, y=50)  
            def destroyer ():
                crear_error.destroy()
            btn297 = Button(crear_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            
    def check_id_error_crear(tabla_proyectos, ID_proyecto, dir_desencriptado):
        print(str(ID_proyecto))
        print(str(dir_desencriptado))
        user = whoami()
        directorio_base = os.path.join("/home", user, "Proyectos")
        print(str(ID_proyecto))
        print(dir_desencriptado)
        carpetas = os.listdir(directorio_base)
        list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        lista_mounted_dir = str(list_mounted_dir.communicate())
        if str(ID_proyecto[-1]) in str(lista_mounted_dir):
            print(str(ID_proyecto[-1]))
            print(str(lista_mounted_dir))
            status = "Montado"
            print("***" + status + "***")
            mostrar_no_error_crear(status)
        else:
            status = "Desmontado"
            print(status)
            mostrar_no_error_crear(status)

    check_id_error_crear(tabla_proyectos, ID_proyecto, dir_desencriptado)
    update(tabla_proyectos)
