#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from complemento import whoami
from actualizar import update

def eliminar_error(tabla_proyectos, item, dir_desencriptado):
    status = 0       
    def mostrar_no_error_elm(status):  
        if status != "Montado":
            eliminar_funciona = Toplevel()
            eliminar_funciona. geometry("500x150")
            eliminar_funciona.configure(bg="#D1D6D6")

            lbl178 = Label(eliminar_funciona, text="¡El proyecto se ha eliminado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl178.place(x=60, y=50)
            def destroyer ():
                eliminar_funciona.destroy()
            btn297 = Button(eliminar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
        else:      
            print("**error**")   
            eliminar_error = Toplevel()
            eliminar_error.geometry("500x150")
            eliminar_error.configure(bg="#D1D6D6")
            lbl278 = Label(eliminar_error, text="¡El proyecto no se ha podido eliminar!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl278.place(x=60, y=50)  
            def destroyer ():
                eliminar_error.destroy()
            btn297 = Button(eliminar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
    def check_id_error_elm(tabla_proyectos, item, dir_desencriptado):
        print(str(item))
        print(str(dir_desencriptado))
        user = whoami()
        directorio_base = os.path.join("/home", user, "Proyectos")
        print(str(item))
        print(dir_desencriptado)
        carpetas = os.listdir(directorio_base)
        list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        lista_mounted_dir = str(list_mounted_dir.communicate())
        if str(item[-1]) in str(lista_mounted_dir):
            print(str(item[-1]))
            print(str(lista_mounted_dir))
            status = "Montado"
            print("***" + status + "***")
            mostrar_no_error_elm(status)
        else:
            status = "Desmontado"
            print(status)
            mostrar_no_error_elm(status)

    check_id_error_elm(tabla_proyectos, item, dir_desencriptado)
    update(tabla_proyectos)