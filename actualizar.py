#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
import time
from subprocess import PIPE
from complemento import whoami

def update(tabla_proyectos):
	def status_carpeta(lista_mounted_dir, carpeta, dir_entero):
		user = whoami()
		conversion = str(carpeta)
		conversion_v2 = conversion + ".zip"

		if dir_entero[-1] in str(lista_mounted_dir):
			status_carpetas = "Montado"
			directorio_backups = os.path.join("/home", user, "backups", conversion_v2)
			comprobacion = os.path.exists(directorio_backups)
			if (comprobacion == False):
				tabla_proyectos.insert("", 0, text=carpeta, values=[dir_entero[-1], status_carpetas, "No existe ningún backup"])
			else:
				directorio_backups = os.path.join("/home", user, "backups", conversion_v2)
				comprobacion = os.path.exists(directorio_backups)

				tiempo1 = os.path.getmtime(directorio_backups)
				tiempo_calculado = time.ctime(tiempo1)
				inicio_tiempo = tiempo_calculado[0:-14]
				final_tiempo = tiempo_calculado [-5:]
				inicio_tiempo_str = str(inicio_tiempo)
				final_tiempo_str = str(final_tiempo)
				tiempo_junto = inicio_tiempo_str + final_tiempo_str
				print (tiempo_junto)
				tabla_proyectos.insert("", 0, text=carpeta, values=[dir_entero[-1], status_carpetas, tiempo_junto])
		else:
			status_carpetas = "Desmontado"
			directorio_backups = os.path.join("/home", user, "backups", conversion_v2)
			comprobacion = os.path.exists(directorio_backups)
			if (comprobacion == True ):
				tiempo1 = os.path.getmtime(directorio_backups)
				tiempo_calculado = time.ctime(tiempo1)
				inicio_tiempo = tiempo_calculado[0:-14]
				final_tiempo = tiempo_calculado [-5:]
				inicio_tiempo_str = str(inicio_tiempo)
				final_tiempo_str = str(final_tiempo)
				tiempo_junto = inicio_tiempo_str + final_tiempo_str
				print (tiempo_junto)
				tabla_proyectos.insert("", 0, text=carpeta, values=[dir_entero[-1], status_carpetas, tiempo_junto])
			else:
				tabla_proyectos.insert("", 0, text=carpeta, values=[dir_entero[-1], status_carpetas, "No existe ningún backup"])

	def check_status(tabla_proyectos):
		user = whoami()
		directorio_base = os.path.join("/home", user, "Proyectos")
		carpetas = os.listdir(directorio_base)
		tabla_proyectos.delete(*tabla_proyectos.get_children())
		list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
		lista_mounted_dir = str(list_mounted_dir.communicate())
		dir_entero = []
		for carpeta in carpetas:
			if not (carpeta[0] == "." and carpeta[-4:] == "_enc"):
				dir_entero.append(os.path.join(directorio_base, carpeta))			
				status_carpeta(lista_mounted_dir, carpeta, dir_entero)
	check_status(tabla_proyectos)
