#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from complemento import whoami
from actualizar import update
#from encriptar import montar_proyecto

def montar_error(tabla_proyectos, item, dir_desencriptado):
    status = 0       
    def mostrar_no_error(status):  
        if status == "Montado":
            encriptar_funciona = Toplevel()
            encriptar_funciona.geometry("500x150")
            encriptar_funciona.minsize(width=500, height=150)
            encriptar_funciona.maxsize(width=500, height=150)
            encriptar_funciona.configure(bg="#D1D6D6")

            lbl178 = Label(encriptar_funciona, text="¡El proyecto se ha montado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl178.place(x=60, y=50)
            def destroyer ():
                encriptar_funciona.destroy()
            btn297 = Button(encriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
        else:      
            print("**error**")   
            encriptar_error = Toplevel()
            encriptar_error.geometry("500x150")
            encriptar_error.minsize(width=500, height=150)
            encriptar_error.maxsize(width=500, height=150)
            encriptar_error.configure(bg="#D1D6D6")
            lbl278 = Label(encriptar_error, text="¡La contraseña no es correcta!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl278.place(x=60, y=50)
            def destroyer ():
                encriptar_error.destroy()

            btn297 = Button(encriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
    def check_id_error(tabla_proyectos, item, dir_desencriptado):
        print("check_id_error" + str(item))
        print("jaja_xd_id" + str(dir_desencriptado))
        user = whoami()
        directorio_base = os.path.join("/home", user, "Proyectos")
        print("id:" + str(item))
        print("dir_item:" + dir_desencriptado)
        carpetas = os.listdir(directorio_base)
        list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        lista_mounted_dir = str(list_mounted_dir.communicate())
        if str(item[-1]) in str(lista_mounted_dir):
            print("-1:" + str(item[-1]))
            print(str(item))
            print(str(lista_mounted_dir))
            status = "Montado"
            print("***" + status + "***")
            mostrar_no_error(status)
        else:
            status = "Desmontado"
            print(status)
            mostrar_no_error(status)

    check_id_error(tabla_proyectos, item, dir_desencriptado)
    update(tabla_proyectos)
