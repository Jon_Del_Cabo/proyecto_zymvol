#encoding=UTF-8
import getpass
from tkinter import *
from tkinter import ttk

def whoami():
	user = getpass.getuser()
	return user

def desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3):
	""" Esto deactiva los botones SI no hay nada seleccionado en la lista de proyectos. """
	btn_1["state"] = DISABLED
	btn_2["state"] = DISABLED
	btn_3["state"] = DISABLED
	btn_1.configure(bg="gray", fg="black")
	btn_2.configure(bg="gray", fg="black")
	btn_3.configure(bg="gray", fg="black")
	print ("Los botones se han desactivado.")
