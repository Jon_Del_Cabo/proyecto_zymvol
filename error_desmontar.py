#encoding=UTF-8
from tkinter import *
from tkinter import ttk
import os
import subprocess
from subprocess import PIPE
from complemento import whoami
from actualizar import update

def desmontar_error(tabla_proyectos, item, dir_desencriptado):
    status = 0       
    def mostrar_no_error_des(status):  
        if status != "Montado":
            desencriptar_funciona = Toplevel()
            desencriptar_funciona.geometry("500x150")
            desencriptar_funciona.minsize(width=500, height=150)
            desencriptar_funciona.maxsize(width=500, height=150)
            desencriptar_funciona.configure(bg="#D1D6D6")

            lbl178 = Label(desencriptar_funciona, text="¡El proyecto se ha desmontado correctamente!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl178.place(x=60, y=50)
            def destroyer ():
                desencriptar_funciona.destroy()
            btn297 = Button(desencriptar_funciona, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
        else:      
            print("**error**")   
            desencriptar_error = Toplevel()
            desencriptar_error.geometry("500x150")
            desencriptar_error.minsize(width=500, height=150)
            desencriptar_error.maxsize(width=500, height=150)
            desencriptar_error.configure(bg="#D1D6D6")
            lbl278 = Label(desencriptar_error, text="¡El proyecto no se ha podido desmontar!", bg="#D1D6D6", fg="black", font=("Arial", 15))
            lbl278.place(x=60, y=50)  
            def destroyer ():
                desencriptar_error.destroy()
            btn297 = Button(desencriptar_error, text="Aceptar", bg="black", fg="white", font=("Arial",10), command=destroyer)
            btn297.place(x=410, y=100)
            update(tabla_proyectos)
            
    def check_id_error_des(tabla_proyectos, item, dir_desencriptado):
        print(str(item))
        print(str(dir_desencriptado))
        user = whoami()
        directorio_base = os.path.join("/home", user, "Proyectos")
        print(str(item))
        print(dir_desencriptado)
        carpetas = os.listdir(directorio_base)
        list_mounted_dir = subprocess.Popen(["df", "--output=target"], stdout=PIPE)
        lista_mounted_dir = str(list_mounted_dir.communicate())
        if str(item[-1]) in str(lista_mounted_dir):
            print(str(item[-1]))
            print(str(lista_mounted_dir))
            status = "Montado"
            print("***" + status + "***")
            mostrar_no_error_des(status)
        else:
            status = "Desmontado"
            print(status)
            mostrar_no_error_des(status)

    check_id_error_des(tabla_proyectos, item, dir_desencriptado)
    update(tabla_proyectos)
