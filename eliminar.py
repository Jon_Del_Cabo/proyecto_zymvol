from tkinter import *
import sys
import os
import subprocess
import getpass
from subprocess import Popen, PIPE, STDOUT
from complemento import whoami, desactivar_botones_si_nada_seleccionado
from actualizar import update
from error_eliminar import eliminar_error

def del_project (tabla_proyectos, btn_1, btn_2, btn_3):

	def eliminar_objeto():
		seleccionado = tabla_proyectos.selection()	
		item = [tabla_proyectos.item(seleccionado)["text"]]
		updated_item = str(item)[2:-2]
		print (updated_item)
		user = whoami()
		directorio_base = os.path.join("/home", user, "Proyectos")
		dir_desencriptado = os.path.join(directorio_base, updated_item)
		dir_encriptado = os.path.join (directorio_base, "." + updated_item + "_enc")
		print(dir_desencriptado)
		print (dir_encriptado)
		subprocess.call(["fusermount", "-uz", dir_desencriptado])
		subprocess.call(["rm", "-rf", dir_desencriptado, dir_encriptado])
		eliminar.destroy()
		update(tabla_proyectos)
		eliminar_error(tabla_proyectos, item, dir_desencriptado)
		desactivar_botones_si_nada_seleccionado(tabla_proyectos, btn_1, btn_2, btn_3)
		
	eliminar = Toplevel()
	eliminar.geometry("500x210")
	eliminar.configure(bg='#D1D6D6')
	eliminar.title("Eliminar proyecto")

	seleccionado = tabla_proyectos.selection()	
	item = [tabla_proyectos.item(seleccionado)["text"]]
	updated_item = str(item)[2:-2]
	print (updated_item)

	texto_confirmacion = "¿Desea eliminar el proyecto " + updated_item + "?"
	print (texto_confirmacion)
	confirmacion = Label(eliminar, bg="#D1D6D6", fg="black", font=("Arial",16), anchor="center", wraplength=400, text= texto_confirmacion)
	confirmacion.place(x=50, y=40, width=400)

	btn77 = Button(eliminar, text="Sí", bg="black", fg="white", command=eliminar_objeto)
	btn77.place(x=280, y=120, width=75)

	def destroyer():
		eliminar.destroy()

	btn78 = Button(eliminar, text="No", bg="black", fg="white", command=destroyer)
	btn78.place(x=140, y=120, width=75)
	
